package com.typeqast.meterapi.dtos;

import com.sun.istack.NotNull;

import lombok.Data;

public @Data class MeterDTO {
	
	private Long id;
	@NotNull
	private String name;
}
