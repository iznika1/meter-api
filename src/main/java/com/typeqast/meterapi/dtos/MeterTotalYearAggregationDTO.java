package com.typeqast.meterapi.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
public @Data class MeterTotalYearAggregationDTO {
	private Integer year;
	private Long total;
}
