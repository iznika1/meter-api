package com.typeqast.meterapi.dtos;

import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
public @Data class MeterYearAggregationPerMonthDTO {
	private Integer year;
	private Map<String, Long> aggregationByMonth = new HashMap<String, Long>();
}
