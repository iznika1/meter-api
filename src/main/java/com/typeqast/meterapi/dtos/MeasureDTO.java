package com.typeqast.meterapi.dtos;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public @Data class MeasureDTO {
	private BigDecimal currentValue;
}
