package com.typeqast.meterapi.controller;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.typeqast.meterapi.domain.Measure;
import com.typeqast.meterapi.domain.Meter;
import com.typeqast.meterapi.dtos.MeasureDTO;
import com.typeqast.meterapi.dtos.MeterDTO;
import com.typeqast.meterapi.dtos.MeterTotalYearAggregationDTO;
import com.typeqast.meterapi.dtos.MeterYearAggregationPerMonthDTO;
import com.typeqast.meterapi.exception.MeterNotFoundException;
import com.typeqast.meterapi.service.MeasureService;
import com.typeqast.meterapi.service.MeterService;

@RestController
@RequestMapping("/meterApi/meters")
public class MeterApiController {

	private ModelMapper modelMapper;
	private MeterService meterService;
	private MeasureService measureService;

	@Autowired
	public MeterApiController(ModelMapper modelMapper, MeterService meterService, MeasureService measureService) {
		this.modelMapper = modelMapper;
		this.meterService = meterService;
		this.measureService = measureService;
	}

	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(value = "/", consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> createMeter(@RequestBody(required = true) MeterDTO meterDTO) {

		try {
			Meter savedMeter = meterService.saveOrUpdate(modelMapper.map(meterDTO, Meter.class));

			URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
					.buildAndExpand(savedMeter.getId()).toUri();
			return ResponseEntity.created(location).build();

		} catch (Exception ex) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Something goes wrong!", ex);
		}
	}

	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(value = "/{meterId}/measures", consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> createMeasure(@PathVariable(required = true) Long meterId,
			@RequestBody(required = true) MeasureDTO measureDTO) {

		Meter existingMeter = meterService.getById(meterId).orElseThrow(MeterNotFoundException::new);
		
		Measure newMeasure = modelMapper.map(measureDTO, Measure.class);
		newMeasure.setMeter(existingMeter);

		Measure savedMeasure = measureService.saveOrUpdate(newMeasure);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedMeasure.getId()).toUri();
		return ResponseEntity.created(location).build();
	}

	@GetMapping("/{meterId}/measures/totalYearAggregation")
	public ResponseEntity<?> getMeterMeasuresTotalYearAggregation(@PathVariable(required = true) Long meterId,
			@RequestParam(name = "year", required = true) Integer year) {

		Long total = measureService.findTotalMeterYearAgregation(year, meterId);
		MeterTotalYearAggregationDTO measureAggregationDTO = new MeterTotalYearAggregationDTO(year, total);

		return ResponseEntity.ok(measureAggregationDTO);
	}

	@GetMapping("/{meterId}/measures/yearAggregationPerMonth")
	public ResponseEntity<?> getMeterMeasuresYearAggregationPerMonth(@PathVariable(required = true) Long meterId,
			@RequestParam(name = "year", required = true) Integer year,
			@RequestParam(name = "month", required = false) Integer month) {

		List<Measure> meterMeasures;
		Map<String, Long> monthsAggregationMap = new HashMap<String, Long>();

		if (month != null) {
			meterMeasures = measureService.findAllByYearAndMonthAndMeter(year, month, meterId);
		} else {
			meterMeasures = measureService.findAllByYearAndMeter(year, meterId);
		}

		monthsAggregationMap = meterMeasures.stream().collect(
				Collectors.groupingBy(measure -> measure.getCreated().getMonth().name(), Collectors.counting()));

		return ResponseEntity.ok(new MeterYearAggregationPerMonthDTO(year, monthsAggregationMap));
	}
}
