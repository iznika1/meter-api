package com.typeqast.meterapi.service;

import java.util.List;

import com.typeqast.meterapi.domain.Measure;

public interface MeasureService extends AbstractCrudService<Measure> {
	List<Measure> findAllByYearAndMeter(Integer year, Long meterId);
	List<Measure> findAllByYearAndMonthAndMeter(Integer year, Integer month, Long meterId);
	Long findTotalMeterYearAgregation(Integer year, Long meterId);
}
