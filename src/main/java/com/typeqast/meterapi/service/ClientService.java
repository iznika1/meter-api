package com.typeqast.meterapi.service;

import com.typeqast.meterapi.domain.Client;

public interface ClientService extends AbstractCrudService<Client> {

}
