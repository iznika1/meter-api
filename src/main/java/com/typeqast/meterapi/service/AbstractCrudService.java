package com.typeqast.meterapi.service;

import java.util.Optional;

public interface AbstractCrudService<T> {
	
	long getTotalSize();
	
	Optional<T> getById(Long id);

	T saveOrUpdate(T t);
}
