package com.typeqast.meterapi.service;

import com.typeqast.meterapi.domain.Meter;

public interface MeterService extends AbstractCrudService<Meter> {

}
