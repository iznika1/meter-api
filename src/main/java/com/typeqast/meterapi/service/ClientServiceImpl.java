package com.typeqast.meterapi.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.typeqast.meterapi.domain.Client;
import com.typeqast.meterapi.repository.ClientRepository;

@Service
@Transactional
public class ClientServiceImpl extends AbstractCrudServiceImpl<Client> implements ClientService {

	@Autowired
	public ClientServiceImpl(ClientRepository repository) {
		super(repository);
	}
}
