package com.typeqast.meterapi.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.typeqast.meterapi.domain.Measure;
import com.typeqast.meterapi.repository.MeasureRepository;

@Service
@Transactional
public class MeasureServiceImpl extends AbstractCrudServiceImpl<Measure> implements MeasureService {

	@Autowired
	public MeasureServiceImpl(MeasureRepository repository) {
		super(repository);
	}

	@Override
	public List<Measure> findAllByYearAndMeter(Integer year, Long meterId) {
		MeasureRepository measureRepository = getRepository();
		return measureRepository.findAllByYearAndMeter(year, meterId);
	}

	@Override
	public List<Measure> findAllByYearAndMonthAndMeter(Integer year, Integer month, Long meterId) {
		MeasureRepository measureRepository = getRepository();
		return measureRepository.findAllByYearAndMonthAndMeter(year, month, meterId);
	}

	@Override
	public Long findTotalMeterYearAgregation(Integer year, Long meterId) {
		MeasureRepository measureRepository = getRepository();
		return measureRepository.findTotalMeterYearAgregation(year, meterId);
	}
}
