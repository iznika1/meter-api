package com.typeqast.meterapi.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.typeqast.meterapi.repository.AbstractCrudRepository;

import lombok.AllArgsConstructor;
import lombok.Data;

@Service
@AllArgsConstructor
public @Data abstract class AbstractCrudServiceImpl<T> implements AbstractCrudService<T> {

	private AbstractCrudRepository<T> repository;
	
	@Override
	public long getTotalSize() {
		return repository.count();
	}
	
	@Override
	public Optional<T> getById(Long id) {
		return repository.findById(id);
	}

	@Override
	public T saveOrUpdate(T t) {
		return repository.save(t);
	}
	
	@SuppressWarnings("unchecked")
	public <S extends AbstractCrudRepository<T>> S getRepository(){
		return (S) repository;
	}
}
