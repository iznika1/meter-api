package com.typeqast.meterapi.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.typeqast.meterapi.domain.Meter;
import com.typeqast.meterapi.repository.MeterRepository;

@Service
@Transactional
public class MeterServiceImpl extends AbstractCrudServiceImpl<Meter> implements MeterService {

	@Autowired
	public MeterServiceImpl(MeterRepository crudRepository) {
		super(crudRepository);
	}
}
