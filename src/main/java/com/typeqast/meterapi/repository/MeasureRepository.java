package com.typeqast.meterapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.typeqast.meterapi.domain.Measure;

public interface MeasureRepository extends AbstractCrudRepository<Measure> {
	@Query("select m from Measure m where year(m.created) = ?1 and meter.id = ?2")
    List<Measure> findAllByYearAndMeter(Integer year, Long meterId);
	
	@Query("select m from Measure m where year(m.created) = ?1 and month(m.created) = ?2 and meter.id = ?3")
	List<Measure> findAllByYearAndMonthAndMeter(Integer year, Integer month, Long meterId);
	
	@Query("select count(1) from Measure m where year(m.created) = ?1 and meter.id = ?2")
    Long findTotalMeterYearAgregation(Integer year, Long meterId);
}
