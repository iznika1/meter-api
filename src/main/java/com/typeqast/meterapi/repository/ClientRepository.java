package com.typeqast.meterapi.repository;

import com.typeqast.meterapi.domain.Client;

public interface ClientRepository extends AbstractCrudRepository<Client> {

}
