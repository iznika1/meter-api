package com.typeqast.meterapi.repository;

import com.typeqast.meterapi.domain.Meter;

public interface MeterRepository extends AbstractCrudRepository<Meter> {

}
