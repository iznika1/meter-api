package com.typeqast.meterapi.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public @Data class Measure extends RootEntity {

	@Column(nullable = false)
	private BigDecimal currentValue;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "METERID", nullable = false)
	private Meter meter;
	
	
}
