package com.typeqast.meterapi.exception;

public class MeterNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 546590299829608070L;

	public MeterNotFoundException() {
		super();
	}

	public MeterNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MeterNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public MeterNotFoundException(String message) {
		super(message);
	}

	public MeterNotFoundException(Throwable cause) {
		super(cause);
	}
}
