package com.typeqast.meterapi;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.typeqast.meterapi.domain.Client;
import com.typeqast.meterapi.domain.Measure;
import com.typeqast.meterapi.domain.Meter;
import com.typeqast.meterapi.service.ClientService;
import com.typeqast.meterapi.service.MeasureService;
import com.typeqast.meterapi.service.MeterService;

@SpringBootApplication
@EnableJpaAuditing
public class MeterApiApplication implements CommandLineRunner {
	
	
	@Autowired private ClientService clientService;
	@Autowired private MeasureService measureService;
	@Autowired private MeterService meterService; 
	
	public static void main(String[] args) {
		SpringApplication.run(MeterApiApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		//insert initial data....				
		if(clientService.getTotalSize() == 0) {
		
			Meter meterOne = new Meter("Meter 1");
			meterService.saveOrUpdate(meterOne);
			
			Measure initMeasureOne = new Measure(new BigDecimal(12312313), meterOne);
			Measure initMeasureTwo = new Measure(new BigDecimal(123123.00), meterOne);
			
			measureService.saveOrUpdate(initMeasureOne);
			measureService.saveOrUpdate(initMeasureTwo);	
			
			Meter initMeterTwo = new Meter("Meter 2");
			meterService.saveOrUpdate(initMeterTwo);	
			
			Client initClient = new Client("Client-1-Address-1234", meterOne);
			clientService.saveOrUpdate(initClient);
		}
	}

}
