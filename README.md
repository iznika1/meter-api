# README #

# Meter api #
Meter api is RESTfull api that will process metering data.
Contains H2 embeded database.

# Concepts in the domain #
					
1. Client - a client residing on a specific address with a smart meter installed at home.
2. Address - the address where the client resides.
3. Meter - a smart meter capable of measuring electricity consumption and keeping historical data about the energy consumption.	

Aplication contains two type of profiles:
1. dev - default profile
2. prod

# Api endpoints:
Executable jar contains embeded tomcat server for deploying application.
Tomcat server startup on 8080 port.
Meter endpoint exposed on /meters path
java -jar meter-api.jar startup application from command line

# GET: Echo
Echo method get information about application endpoint status.

http://{host}:8080/meterApi/meters/echo

# POST Create meter
Create meter method is POST method for create new meter.
Method return 201 created if meter is successfully created.

http://{host}:8080/meterApi/meters/
```
Request body:
{
    "name" : "New meter2" <- your value
}
```

# POST Create measure
Create measure method is POST method for create meter measure.
Method return 201 created if measure is successfully created.

meterId - path parameter
http://{host}:8080/meterApi/meters/{meterId}/measures
```
Request body:
{
	"currentValue" : 2131.92 <- your value
}
```

# GET Get meter total year aggregation
Method get meter total year aggregation.

meterIdValue - path parameter
yearValue - request/query parameter
http://{host}:8080/meterApi/meters/{meterIdValue}/measures/totalYearAggregation?year={yearValue}

ResponseBody:
```
{
    "year": 2020,
    "total": 0
}
```

# Get meter year aggregation per month
Method het meter year aggregation per month

meterIdValue - path parameter
yearValue - request/query parameter
http://{host}:8080/meterApi/meters/{meterIdValue}/measures/yearAggregationPerMonth?year={yearValue}

Reponse body:
```
{
    "year": 2020,
    "aggregationByMonth": {
        "AUGUST": 2,
		"SEPTEMBER": 3
    }
}
```

or
For specific month in year
meterIdValue - path parameter
yearValue - request/query parameter
monthValue - request/query parameter

http://{host}:8080/meterApi/meters/{meterId}/measures/yearAggregationPerMonth?year={yearValue}&month={monthValue}

Reponse body:
```
{
    "year": 2020,
    "aggregationByMonth": {
        "AUGUST": 2,
    }
}
```


